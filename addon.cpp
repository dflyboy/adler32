#include <nan.h>
#include <cstdio>
#include <cryptopp/adler32.h>
#include <cryptopp/base64.h>

using namespace std;
using namespace Nan;
using namespace v8;

class HashWorker : public AsyncWorker
{
  public:
    HashWorker(Callback *callback, char *data, size_t len)
        : AsyncWorker(callback), data(data), len(len) {}

    ~HashWorker() {}

    void Execute()
    {
        CryptoPP::Adler32 algo;
        // CryptoPP::SHA1 algo;

        algo.Update((unsigned char *)data, len);
        unsigned int size = algo.DigestSize();
        unsigned char hash[size];
        algo.TruncatedFinal(hash, size);

        CryptoPP::Base64Encoder encoder(nullptr, false);
        encoder.Put(hash, size);
        encoder.MessageEnd();

        CryptoPP::word64 osize = encoder.MaxRetrievable();

        encoded.resize(osize);
        encoder.Get((unsigned char *)&encoded[0], encoded.size());
    }

    // We have the results, and we're back in the event loop.
    void HandleOKCallback()
    {
        // Nan::HandleScope scope;

        Local<Value> argv[] = {
            Nan::New<String>(encoded.c_str()).ToLocalChecked()};

        callback->Call(1, argv);
    }

  private:
    char *data;
    size_t len;
    std::string encoded;
};

NAN_METHOD(HashSync)
{
    // Nan::HandleScope scope;
    // Make property names to access the input object

    if (info.Length() < 1)
    {
        return;
    }
    if (!(info[0]->IsString() || info[0]->IsObject()))
    {
        return;
    }

    char *data = node::Buffer::Data(info[0]->ToObject());
    size_t len = node::Buffer::Length(info[0]->ToObject());

    CryptoPP::Adler32 algo;
    // CryptoPP::SHA1 algo;

    algo.Update((unsigned char *)data, len);
    unsigned int size = algo.DigestSize();
    unsigned char hash[size];
    algo.TruncatedFinal(hash, size);

    string encoded;

    CryptoPP::Base64Encoder encoder(nullptr, false);
    encoder.Put(hash, size);
    encoder.MessageEnd();

    CryptoPP::word64 osize = encoder.MaxRetrievable();

    encoded.resize(osize);
    encoder.Get((unsigned char *)&encoded[0], encoded.size());

    info.GetReturnValue().Set(Nan::New<String>(encoded.c_str()).ToLocalChecked());
}

NAN_METHOD(HashAsync)
{
    // Nan::HandleScope scope;
    if (info.Length() < 1)
    {
        return;
    }
    if (!(info[0]->IsString() || info[0]->IsObject()))
    {
        return;
    }

    char *data = node::Buffer::Data(info[0]->ToObject());
    size_t len = node::Buffer::Length(info[0]->ToObject());

    Callback *callback = new Callback(info[1].As<Function>());

    AsyncQueueWorker(new HashWorker(callback, data, len));
}

NAN_MODULE_INIT(Init)
{
    Nan::Set(target, New<String>("adler32").ToLocalChecked(),
             GetFunction(New<FunctionTemplate>(HashSync)).ToLocalChecked());
    Nan::Set(target, New<String>("adler32async").ToLocalChecked(),
             GetFunction(New<FunctionTemplate>(HashAsync)).ToLocalChecked());
}

// macro to load the module when require'd
NODE_MODULE(my_addon, Init)
const fs = require('fs');

var binding = require('./build/Release/adler32');

fs.readFile(process.argv[2], (e, data) => {
    const hash = binding.adler32(data);
    console.log(hash);

    binding.adler32async(data, result => {
        console.log(result);
    })
});